import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './pages/root/app.component';
import { RoomComponent } from './pages/room/room.component';
import { HomeComponent } from './pages/home/home.component';
import { CloudComponent } from './pages/cloud/cloud.component';
import { ErrorComponent } from './pages/error/error.component';
import { SettingsComponent } from './pages/settings/settings.component';
import {FormsModule} from "@angular/forms";
import { LoginComponent } from './pages/login/login.component';
import {HttpClientModule} from "@angular/common/http";
import { CloudFilePreviewComponent } from './pages/cloud-file-preview/cloud-file-preview.component';
import { CloudFileRenameComponent } from './pages/cloud-file-rename/cloud-file-rename.component';
import { CloudFileShareComponent } from './pages/cloud-file-share/cloud-file-share.component';
import { CloudFileShareLinkComponent } from './pages/cloud-file-share-link/cloud-file-share-link.component';
import { CloudFileDetailsComponent } from './pages/cloud-file-details/cloud-file-details.component';
import { CloudFileDeleteComponent } from './pages/cloud-file-delete/cloud-file-delete.component';
import { CloudFileStopSharingComponent } from './pages/cloud-file-stop-sharing/cloud-file-stop-sharing.component';
import { CallComponentComponent } from './pages/call-component/call-component.component';
import { ProfileViewComponent } from './pages/profile-view/profile-view.component';
import { ProfileListComponent } from './pages/profile-list/profile-list.component';
import { ProfileRemoveComponent } from './pages/profile-remove/profile-remove.component';
import { CreateRoomComponent } from './pages/create-room/create-room.component';
import { DeleteRoomComponent } from './pages/delete-room/delete-room.component';
import { EditRoomComponent } from './pages/edit-room/edit-room.component';
import { ProfileEditComponent } from './pages/profile-edit/profile-edit.component';
import { CreateRoomComponentComponent } from './pages/create-room-component/create-room-component.component';

@NgModule({
  declarations: [
    AppComponent,
    RoomComponent,
    HomeComponent,
    CloudComponent,
    ErrorComponent,
    SettingsComponent,
    LoginComponent,
    CloudFilePreviewComponent,
    CloudFileRenameComponent,
    CloudFileShareComponent,
    CloudFileShareLinkComponent,
    CloudFileDetailsComponent,
    CloudFileDeleteComponent,
    CloudFileStopSharingComponent,
    CallComponentComponent,
    ProfileViewComponent,
    ProfileListComponent,
    ProfileRemoveComponent,
    CreateRoomComponent,
    DeleteRoomComponent,
    EditRoomComponent,
    ProfileEditComponent,
    CreateRoomComponentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
