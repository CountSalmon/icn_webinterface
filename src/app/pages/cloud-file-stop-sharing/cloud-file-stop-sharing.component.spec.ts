import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloudFileStopSharingComponent } from './cloud-file-stop-sharing.component';

describe('CloudFileStopSharingComponent', () => {
  let component: CloudFileStopSharingComponent;
  let fixture: ComponentFixture<CloudFileStopSharingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloudFileStopSharingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudFileStopSharingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
