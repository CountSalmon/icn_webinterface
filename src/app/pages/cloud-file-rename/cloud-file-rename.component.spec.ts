import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloudFileRenameComponent } from './cloud-file-rename.component';

describe('CloudFileRenameComponent', () => {
  let component: CloudFileRenameComponent;
  let fixture: ComponentFixture<CloudFileRenameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloudFileRenameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudFileRenameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
