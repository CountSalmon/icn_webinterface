import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-cloud-file-rename',
  templateUrl: './cloud-file-rename.component.html',
  styleUrls: ['./cloud-file-rename.component.css']
})
export class CloudFileRenameComponent implements OnInit {

  public cloudFile = {
    title: "config.json"
  };

  public givenFileId = "";

  constructor(private router: Router, private http: HttpClient, private route: ActivatedRoute) {
    this.route.paramMap.subscribe(params => {
      this.givenFileId = params.get("fileId");
    });
  }

  ngOnInit(): void {
  }

  public navigateToPreview(): void {
    this.router.navigate(["/cloud/" + this.givenFileId + "/preview"]);
  }

  public saveNewName(newName: string): void {
    if (newName.length > 0) {
      console.log(newName);
      this.navigateToPreview();
    }

  }

}
