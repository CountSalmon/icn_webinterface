import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {BackendRequest} from "../../scripts/BackendRequest";
import {HttpClient} from "@angular/common/http";
import {MessageHandler} from "../../scripts/MessageHandler";

@Component({
  selector: 'app-call-component',
  templateUrl: './call-component.component.html',
  styleUrls: ['./call-component.component.css']
})
export class CallComponentComponent implements OnInit {

  public componentName: string = "";
  public component: object = {};
  public componentCallObject: object = {
    color: false
  };

  private callBuildObject: object = {};
  private givenComponentId: string;
  private givenRoomId: string;

  constructor(private router: Router, private http: HttpClient, private route: ActivatedRoute) {
    route.paramMap.subscribe(params => {
      this.givenComponentId = params.get("componentId");
      this.givenRoomId = params.get("roomId");
    });
    this.setComponent();
    this.setComponentCallInstructions();
  }

  ngOnInit(): void {
  }

  private setComponent(): void {
    const componentNameRequest: BackendRequest = new BackendRequest(
      "/api/component/" + this.givenComponentId,
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    const resultPromise = componentNameRequest.execute().toPromise();
    resultPromise.then(result => {
      this.component = result;
    });
  }

  private setComponentCallInstructions(): void {
    const componentInstructionsRequest: BackendRequest = new BackendRequest(
      "/api/component/call/" + this.givenComponentId,
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    const requestPromise = componentInstructionsRequest.execute().toPromise();
    requestPromise.then(result => {
      this.componentCallObject = result;
    });
  }

  public getComponentName(): string {
    // @ts-ignore
    return this.component.name;
  }

  public getColor(): boolean {
    // @ts-ignore
    return this.componentCallObject.color;
  }

  public hasButtons(): boolean {
    return this.getButtons() != null;
  }

  public getButtons(): object {
    // @ts-ignore
    return this.componentCallObject.buttons;
  }

  public hasInputs(): boolean {
    return this.getInputs() != null;
  }

  public getInputs(): object {
    // @ts-ignore
    return this.componentCallObject.inputs;
  }

  public setButtonValue(key: string, value: string): void {
    this.callBuildObject[key] = value;
  }

  public setInputValue(key: string, event: Event): void {
    // @ts-ignore
    this.callBuildObject[key] = event.target.value;
  }

  public setColorValue(event: Event): void {
    // @ts-ignore
    const rgbArray = this.hexToRGB(event.target.value);
    // @ts-ignore
    this.callBuildObject.red = rgbArray[0];
    // @ts-ignore
    this.callBuildObject.green = rgbArray[1];
    // @ts-ignore
    this.callBuildObject.blue = rgbArray[2];
  }

  public sendCall(navigateBack: boolean): void {
    const callRequest: BackendRequest = new BackendRequest(
      "/api/call/component/" + this.givenComponentId,
      "post",
      this.http,
      {},
      {
        content: this.callBuildObject
      },
      "json"
    ).setDefaultHeaders();
    const callPromise = callRequest.execute().toPromise();
    callPromise.then(result => {
      if (navigateBack) {
        // @ts-ignore
        this.router.navigate(["/room/" + this.component.roomId + "/view"]);
      }
    });
  }

  public deleteComponent(): void {
    if (confirm("Are you sure that you want to delete that component?")) {
      const deleteComponentRequest: BackendRequest = new BackendRequest(
        "/api/component/" + this.givenComponentId,
        "delete",
        this.http,
        {},
        {},
        "json"
      ).setDefaultHeaders();
      deleteComponentRequest.execute().subscribe(result => {
        MessageHandler.sendSuccess("Deleted the selected roomComponent!", 3000);
        this.router.navigate([`/room/${this.givenRoomId}/view`]);
      });
    }
  }

  public hexToRGB(hex: string): object {
    const r = parseInt(hex.slice(1, 3), 16);
    const g = parseInt(hex.slice(3, 5), 16);
    const b = parseInt(hex.slice(5, 7), 16);
    return [r, g, b];
  }

}
