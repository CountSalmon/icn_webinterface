import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {BackendRequest} from "../../scripts/BackendRequest";
import {Credentials} from "../../scripts/Credentials";
import {MessageHandler} from "../../scripts/MessageHandler";

@Component({
  selector: 'app-delete-room',
  templateUrl: './delete-room.component.html',
  styleUrls: ['./delete-room.component.css']
})
export class DeleteRoomComponent implements OnInit {

  public givenRoomId;
  public roomName;
  private roomObject;

  constructor(private router: Router, private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.givenRoomId = params.get("roomId");
      this.setRoomData();
      this.checkUserPermission();
    });
  }

  public deleteRoom(): void {
    const deleteRoomRequest: BackendRequest = new BackendRequest(
      "/api/room/" + this.givenRoomId,
      "delete",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    deleteRoomRequest.execute().toPromise().then(result => {
      MessageHandler.sendSuccess("deleted the room '" + this.roomObject.name + "'", 3000);
      this.router.navigate(["/home"]);
    });
  }

  private setRoomData(): void {
    const getRoomsByUserRequest: BackendRequest = new BackendRequest(
      "/api/room/" + this.givenRoomId,
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    getRoomsByUserRequest.execute().toPromise().then(result => {
      this.roomObject = result;
      // @ts-ignore
      this.roomName = result.name;
    });
  }

  public checkUserPermission(): void {
    const getPermissionsRequest: BackendRequest = new BackendRequest(
      `/api/room/permission/room/${this.givenRoomId}`,
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    getPermissionsRequest.execute().toPromise().then(result => {
      let contains: boolean = false;
      // @ts-ignore
      result.forEach(userpermission => {
        if (userpermission.accountId === Credentials.getUser().id) {
          contains = true;
        }
      });
      if (!contains) {
        MessageHandler.sendError("You dont h ave the permission to access that area!", 3000);
        this.router.navigate(["/home"]);
      }
    });
  }

}
