import {Component, HostListener, Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Credentials} from "../../scripts/Credentials";
import {Router} from "@angular/router";
import {BackendRequest} from "../../scripts/BackendRequest";
import {MessageHandler} from "../../scripts/MessageHandler";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

@Injectable()
export class LoginComponent implements OnInit {

  constructor(private router: Router, private http: HttpClient) {

  }

  ngOnInit(): void {
  }

  public submit(username: string, password: string): void {
    Credentials.setUserName(username);
    Credentials.setPassword(password);
    const loginRequest: BackendRequest = new BackendRequest("/api/login", "get", this.http, {}, null, "text");
    loginRequest.setDefaultHeaders();
    console.log(loginRequest.getHeaders());
    loginRequest.execute().pipe().subscribe(result => {
      // @ts-ignore
      if (result === "ok") {
        const getUserRequest: BackendRequest = new BackendRequest(
          "/api/user/self",
          "get",
          this.http,
          {},
          {},
          "json"
        ).setDefaultHeaders();
        getUserRequest.execute().subscribe(user => {
          Credentials.setUser(user);
          MessageHandler.sendSuccess("successfully logged in", 3000);
          this.router.navigate(["/home"]);
        });
      } else {
        MessageHandler.sendError("email is not matching with password", 3000);
      }
    });
  }


  @HostListener("document:keypress", ["$event"])
  // tslint:disable-next-line:typedef
  public handleKeyboardEvent(event: KeyboardEvent) {
    switch (event.key) {
      case  "Enter":
        const usernameInput = document.getElementById("username-input");
        const passwordInput = document.getElementById("password-input");
        // @ts-ignore
        this.submit(usernameInput.value, passwordInput.value);
        break;
    }
  }

}
