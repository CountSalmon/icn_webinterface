import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BackendRequest} from "../../scripts/BackendRequest";
import {MessageHandler} from "../../scripts/MessageHandler";
import {JSONCompletion} from "../../scripts/JSONCompletion";

@Component({
  selector: 'app-create-room-component',
  templateUrl: './create-room-component.component.html',
  styleUrls: ['./create-room-component.component.css']
})
export class CreateRoomComponentComponent implements OnInit {

  public givenRoomId: string;

  // component variables
  public buildInstructions: object;
  public types;
  public componentBase: object = {};
  public selectedType;

  // theme variables
  public themeElements: Array<object> = [];
  public roomComponents: object = [];
  public selectedComponentId;

  constructor(private router: Router, private http: HttpClient, private route: ActivatedRoute) {
    this.loadPluginTypes();
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.givenRoomId = params.get("roomId");
      this.setRoomComponents();
    });
  }

  public setRoomComponents(): void {
    const getRoomComponentsRequest: BackendRequest = new BackendRequest(
      `/api/component/room/${this.givenRoomId}`,
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    getRoomComponentsRequest.execute().subscribe(result => {
      this.roomComponents = result;
    });
  }

  public addThemeElement(): void {
    // @ts-ignore
    this.themeElements.push(
      {
        id: this.themeElements.length + 1,
        sequence: "",
        delay: 0,
      });
  }

  public deleteThemeElement(themeElementId: string): void {
    // @ts-ignore
    this.themeElements.forEach(themeElement => {
      // @ts-ignore
      if (themeElement.id === themeElementId) {
        const index = this.themeElements.indexOf(themeElement);
        if (index > -1) {
          this.themeElements.splice(index, 1);
        }
      }
    });
  }

  public async completeContent(keyPressed: string, contentElement: HTMLElement): Promise<void> {
    // @ts-ignore
    let currentValue = contentElement.value;
    const newKey = JSONCompletion.getOutputKey(keyPressed);
    if (newKey.length > 0) {
      // @ts-ignore
      currentValue = currentValue.substring(0, contentElement.selectionStart)
        + newKey
        // @ts-ignore
        + currentValue.substring(contentElement.selectionEnd, currentValue.length);
      // @ts-ignore
      contentElement.value = currentValue;
      // @ts-ignore
      contentElement.selectionStart = contentElement.selectionStart + newKey.length;
    }
  }

  public beautifyJSON(elementId: string, contentElement: HTMLElement): void {
    try {
      // @ts-ignore
      const obj = JSON.parse(contentElement.value);
      const newValue = JSON.stringify(obj, null, 2);
      // @ts-ignore
      contentElement.value = newValue;
      this.setThemeObjectValue(elementId, 'contents', obj);
    } catch (error) {
      console.log("JSON is not valid!");
    }

  }

  public setThemeObjectValue(elementId: string, key: string, value: any): void {
    this.themeElements.forEach(themeElement => {
      // @ts-ignore
      if (themeElement.id === elementId) {
        themeElement[key] = value;
      }
      console.log(themeElement);
    });
  }

  public createRoomElement(choice: string, componentName: string, thumbnail: FileList): void {

    if (!componentName) {
      MessageHandler.sendError("Title input mustn't be empty!", 3000);
      return;
    }
    if (!thumbnail) {
      MessageHandler.sendError("Please upload a thumbnail!", 3000);
      return;
    }
    // upload thumbnail
    const givenFile = thumbnail.item(0);
    const formData = new FormData();
    formData.append("file", thumbnail.item(0));
    const uploadThumbnailRequest: BackendRequest = new BackendRequest(
      "/api/thumbnail",
      "post",
      this.http,
      {},
      formData,
      "text"
    ).setDefaultHeaders();
    const thumbnailPromise = uploadThumbnailRequest.execute().toPromise();
    thumbnailPromise.then(result => {

      switch (choice) {
        case "component":
          const createComponentRequest: BackendRequest = new BackendRequest(
            "/api/component",
            "post",
            this.http,
            {},
            {
              name: componentName,
              roomId: this.givenRoomId,
              imagePath: result,
              componentBase: this.componentBase,
              type: this.selectedType
            },
            "json"
          ).setDefaultHeaders();
          const createPromise = createComponentRequest.execute().toPromise();
          createPromise.then(themeResult => {
            console.log(themeResult);
          });
          break;
        case "theme":
          const createThemeRequest: BackendRequest = new BackendRequest(
            "/api/theme",
            "post",
            this.http,
            {},
            {
              roomId: this.givenRoomId,
              name: componentName,
              imagePath: result,
              content: this.themeElements,
            },
            "json"
          ).setDefaultHeaders();
          createThemeRequest.execute().subscribe(themeResult => {
            this.router.navigate([`/room/${this.givenRoomId}/view`]);
          });
          break;
      }
    });
  }

  public setInput(inputKey: string): void {
    const inputElement = document.getElementById(inputKey);
    if (inputElement) {
      // @ts-ignore
      this.componentBase[inputKey] = inputElement.value;
      console.log(this.componentBase);
    }
  }

  public getInputs(): object {
    // @ts-ignore
    return this.buildInstructions ? this.buildInstructions.inputs : null;
  }

  public getChoices(): object {
    // @ts-ignore
    return this.buildInstructions.choices ? this.buildInstructions.choices : null;
  }

  public loadPluginTypes(): void {
    const getTypeRequest: BackendRequest = new BackendRequest(
      "/api/type/all/names",
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    const resultPromise = getTypeRequest.execute().toPromise();
    resultPromise.then(result => {
      // @ts-ignore
      this.types = result.types;
    });
  }

  public loadComponentInstructions(selectedTypeName: string): void {
    if (selectedTypeName) {
      const getBuildInstructions: BackendRequest = new BackendRequest(
        `/api/component/setup/${selectedTypeName}`,
        "get",
        this.http,
        {},
        {},
        "json"
      ).setDefaultHeaders();
      getBuildInstructions.execute().toPromise().then(result => {
        this.buildInstructions = result;
      });
    }
  }
}
