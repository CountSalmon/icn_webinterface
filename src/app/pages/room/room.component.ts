import {Component, OnInit, HostListener, Directive, Output, EventEmitter, NgZone} from '@angular/core';
import {Utils} from "../../scripts/Utils";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {BackendRequest} from "../../scripts/BackendRequest";
import {Credentials} from "../../scripts/Credentials";
import {MessageHandler} from "../../scripts/MessageHandler";


// @ts-ignore
@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {

  public notesText = "";

  public selectedComponent = {
    color: true
  };

  public roomElements = [];
  public themeElements = [];
  public reminderElements = [];
  public givenRoomId;

  constructor(private router: Router, private http: HttpClient, private route: ActivatedRoute, private zone: NgZone) {
  }

  ngOnInit(): void {
    // get room id from url
    // reloads the content if the route changes. The component just gets rendered once and ignores route param changes
    this.route.paramMap.subscribe(params => {
      this.givenRoomId = params.get("roomId");
      this.initToggleOptions();
      this.getRoomNoteContent();
      this.getRoomReminders();
      this.getRoomElements();
      this.getRoomThemes();
    });
    // user has permission to edit that account
    if (!this.userHasRoomPermission()) {
      this.router.navigate(["/home"]);
    }
  }

  public callTheme(content): void {
    const callThemeRequest: BackendRequest = new BackendRequest(
      `/api/call/theme/${content.id}`,
      "post",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    callThemeRequest.execute().subscribe(result => {
      console.log(result);
    });
  }

  /**
   * toggle options under the themes (e.g. music, notes, reminder)
   */
  public initToggleOptions(): void {
    const optionList = document.getElementById("others-select");
    // click function caused some style errors
    // tslint:disable-next-line:only-arrow-functions typedef
    optionList.addEventListener("mouseup", function (event) {
      const children = optionList.childNodes;
      // tslint:disable-next-line:only-arrow-functions typedef
      children.forEach(function(child) {
        // @ts-ignore
        child.className = "";
      });
      // @ts-ignore
      event.target.className = "horizontal-select-selected";
    });
  }

  public addReminderElement(reminderContent: string): void {
    if (reminderContent) {
      const addReminderElementRequest: BackendRequest = new BackendRequest(
        "/api/room/reminder/" + this.givenRoomId,
        "post",
        this.http,
        {},
        {content: reminderContent},
        "json"
      ).setDefaultHeaders();
      addReminderElementRequest.execute().toPromise().then(success => {
        MessageHandler.sendSuccess("reminder has been saved", 1000);
        this.getRoomReminders();
      });
    }
  }

  public deleteReminderElement(elementId: string): void {
    const deleteReminderElementRequest: BackendRequest = new BackendRequest(
      "/api/room/reminder/" + this.givenRoomId + "/" + elementId,
      "delete",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    deleteReminderElementRequest.execute().toPromise().then(success => {
      MessageHandler.sendSuccess("deleted room reminder", 1000);
      this.getRoomReminders();
    });
  }

  @HostListener("document:keypress", ["$event"])
  // tslint:disable-next-line:typedef
  public handleKeyboardEvent(event: KeyboardEvent) {
    switch (event.key) {
      case  "Enter":
        const reminderInputElement = document.getElementById("reminder-input-field");
        if (reminderInputElement === document.activeElement) {
          // @ts-ignore
          this.addReminderElement(reminderInputElement.value);
          // @ts-ignore
          reminderInputElement.value = "";
        }
        break;
    }
  }

  public saveNoteContent(roomNoteText: string): void {
    const roomNoteSaveRequest: BackendRequest = new BackendRequest(
      "/api/room/note/" + this.givenRoomId,
      "post",
      this.http,
      {},
      {
        content: roomNoteText
      },
      "json"
    ).setDefaultHeaders();
    roomNoteSaveRequest.execute().toPromise().catch(error => {
      MessageHandler.sendError("couldn't save the room note.", 3000);
    });
  }

  public sendMusicURL(text: string): void {
    console.log(Utils.isURL(text));
  }

  // @ts-ignore
  private async userHasRoomPermission(): boolean {
    const roomPermissionRequest: BackendRequest = new BackendRequest(
      "/api/room/permission/room/" + this.givenRoomId,
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    const response: any = await roomPermissionRequest.execute().toPromise();
    response.forEach(currentElement => {
      if (currentElement.id === Credentials.getUser().id) {
        return true;
      }
    });
    return false;
  }

  private getRoomNoteContent(): void {
    const roomNoteRequest: BackendRequest = new BackendRequest(
      "/api/room/note/" + this.givenRoomId,
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    roomNoteRequest.execute().toPromise().then(response => {
      // @ts-ignore
      this.notesText = response.noteContent;
    });
  }

  private getRoomReminders(): void {
    const roomReminderRequest: BackendRequest = new BackendRequest(
      "/api/room/reminder/" + this.givenRoomId,
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    roomReminderRequest.execute().toPromise().then(response => {
      // @ts-ignore
      this.reminderElements = response;
    });
  }

  private getRoomElements(): void {
    const getRoomComponentsRequest: BackendRequest = new BackendRequest(
      "/api/component/room/" + this.givenRoomId,
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    const requestPromise = getRoomComponentsRequest.execute().toPromise();
    requestPromise.then(result => {
      // @ts-ignore
      this.roomElements = result;
    });
  }

  public getRoomThemes(): void {
    const getRoomThemesRequest: BackendRequest = new BackendRequest(
      `/api/theme/room/${this.givenRoomId}`,
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    getRoomThemesRequest.execute().subscribe(result => {
      // @ts-ignore
      this.themeElements = result;
    });
  }

}
