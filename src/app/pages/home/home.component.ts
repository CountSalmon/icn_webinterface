import {Component, OnInit} from '@angular/core';
import {Credentials} from "../../scripts/Credentials";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {BackendRequest} from "../../scripts/BackendRequest";
import {RoomHandler} from "../../scripts/RoomHandler";
import {Authorization} from "../../scripts/Authorization";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router, private http: HttpClient, private route: ActivatedRoute) {
    if (Authorization.checkForLogin(this.router)) {
      HomeComponent.setSidebarData(this.http);
    }
  }

  public static setSidebarData(http: HttpClient): void {
    if (Credentials.getUser()) {
      const getRoomRequest: BackendRequest = new BackendRequest(
        "/api/room/user/" + Credentials.getUser().id,
        "get",
        http,
        {},
        {},
        "json"
      ).setDefaultHeaders();
      getRoomRequest.execute().pipe().subscribe(rooms => {
        RoomHandler.givenRooms = rooms;
      });
    }
  }

  ngOnInit(): void {
  }

}
