import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileRemoveComponent } from './profile-remove.component';

describe('ProfileRemoveComponent', () => {
  let component: ProfileRemoveComponent;
  let fixture: ComponentFixture<ProfileRemoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileRemoveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileRemoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
