import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloudFileDeleteComponent } from './cloud-file-delete.component';

describe('CloudFileDeleteComponent', () => {
  let component: CloudFileDeleteComponent;
  let fixture: ComponentFixture<CloudFileDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloudFileDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudFileDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
