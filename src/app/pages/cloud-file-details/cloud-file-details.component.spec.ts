import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloudFileDetailsComponent } from './cloud-file-details.component';

describe('CloudFileDetailsComponent', () => {
  let component: CloudFileDetailsComponent;
  let fixture: ComponentFixture<CloudFileDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloudFileDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudFileDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
