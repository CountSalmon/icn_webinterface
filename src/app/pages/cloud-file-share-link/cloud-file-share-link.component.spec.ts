import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloudFileShareLinkComponent } from './cloud-file-share-link.component';

describe('CloudFileShareLinkComponent', () => {
  let component: CloudFileShareLinkComponent;
  let fixture: ComponentFixture<CloudFileShareLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloudFileShareLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudFileShareLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
