import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Router} from "@angular/router";
import {MessageHandler} from "../../scripts/MessageHandler";
import {Credentials} from "../../scripts/Credentials";
import {Authorization} from "../../scripts/Authorization";
import {BackendRequest} from "../../scripts/BackendRequest";

@Component({
  selector: 'app-create-room',
  templateUrl: './create-room.component.html',
  styleUrls: ['./create-room.component.css']
})
export class CreateRoomComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) {
    Authorization.checkForLogin(router);
  }

  ngOnInit(): void {
  }

  public createRoom(roomName: string): void {
    if (roomName && roomName.length > 0) {
      const createRoomRequest: BackendRequest = new BackendRequest(
        "/api/room",
        "post",
        this.http,
        {params: new HttpParams().set("roomName", roomName)},
        {},
        "json"
      ).setDefaultHeaders();
      createRoomRequest.execute().toPromise().then(result => {
        this.router.navigate(["/home"]);
        MessageHandler.sendSuccess("room " + roomName + " has been created!", 3000);
      });
    } else {
      MessageHandler.sendError("room name cannot be empty", 3000);
    }
  }

}
