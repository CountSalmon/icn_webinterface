import {Component, OnInit, Renderer2} from '@angular/core';
import {Router} from "@angular/router";
import {BackendRequest} from "../../scripts/BackendRequest";
import {HttpClient} from "@angular/common/http";
import {Credentials} from "../../scripts/Credentials";

@Component({
  selector: 'app-cloud',
  templateUrl: './cloud.component.html',
  styleUrls: ['./cloud.component.css']
})
export class CloudComponent implements OnInit {

  private selectedFolders = new Set<string>();
  public folderMap = new Map<string, string>();

  private selectedFiles = new Set<string>();
  private fileMap = new Map<string, string>();

  private typeObject = new Map<string, string>();

  public contextX: number = 150;
  public contextY: number = 20;

  public folderColors = [
    "#595959",
    "#eb4034",
    "#7d0f07",
    "#fa7719",
    "#5fbd1c",
    "#3cdece",
    "#086da3",
    "#2860d1",
    "#7c44e3",
    "#9a1dde",
    "#d41fde",
    "#cf13a3",
  ];

  constructor(private render: Renderer2, private router: Router, private http: HttpClient) {
    this.typeObject.set("dev", "computer");
    this.typeObject.set("image", "photo");
    this.typeObject.set("video", "movie_creation");
    this.typeObject.set("txt", "sticky_note_2");
    this.typeObject.set("none", "polymer");

  }

  public pathObject = [
    {
      id: "naskdnlandakdandnlas",
      name: "SimonSchwedes"
    },
    {
      id: "sadnasdasdd",
      name: "private"
    },
    {
      id: "dsadkandasdsdad",
      name: "business"
    }
  ];

  public folderObject = [
    {
      id: "dnaklnnxldnadsada",
      name: "Pascal Paradigma",
      author: "23231903821093890123190273901",
      color: "#7d0f07",
    },
    {
      id: "asddsadasdas",
      name: "Max Muster",
      author: "23231903821093890123190273901",
      color: "#7c44e3",
    },
    {
      id: "sddsdsdsdsdsdsdsdsdsdsdsds",
      name: "Valentin Vorbild",
      author: "23231903821093890123190273901",
      color: null,
    },
    {
      id: "dnaklnnxldnsddsddsdsdsdsdsdsdsdsdadsada",
      name: "Markus Modell",
      author: "23231903821093890123190273901",
      color: "#eb4034",
    },
  ];

  public fileObject = [
    {
      id: "sdsalndkasdmasmdas",
      name: "test.txt",
      type: "txt",
      author: "31293780173203710933701"
    },
    {
      id: "oisdjassadasdklndksaldsa",
      name: "config.json",
      type: "dev",
      author: "9weqw9910283020129031903819023"
    },
    {
      id: "oisdjaskssslndksaldsa",
      name: "plugin.json",
      type: "none",
      author: "9weqw9910283020129031903819023"
    },
    {
      id: "oisdjasklnsdadasdsdsdsdsddksaldsa",
      name: "test_image.jpg",
      type: "image",
      author: "9weqw9910283020129031903819023"
    },
    {
      id: "oisdjasklndksaldffdgdfsa",
      name: "IcnAPI.toml",
      type: "dev",
      author: "9weqw9910283020129031903819023"
    },
    {
      id: "ssddsdassdasdasdadsdadasdadadasd",
      name: "video.mp4",
      type: "video",
      author: "9weqw9910283020129031903819023"
    },
  ];

  ngOnInit(): void {
    if (!Credentials.areGiven()) {
      this.router.navigate(["/login"]);
    }
    this.folderObject.forEach(folderElement => {
      this.folderMap.set(folderElement.id, "folder");
    });

    this.fileObject.forEach(fileElement => {
      this.fileMap.set(fileElement.id, fileElement.type);
    });
  }

  public navigateToFolder(folderId: string, source: string): void {
    if (source === "path") {
    } else if (source === "view") {
      if (this.selectedFolders.size !== 0) { // can't navigate into one folder because multiple were selected
      } else {
      }
      return;
    }
  }

  public selectFolderElements(folderId: string): void {
    if (this.selectedFolders.has(folderId)) {
      this.unselectFolder(folderId);
      return;
    }
    this.selectFolder(folderId);
  }

  public unselectFolder(folderId: string): void {
    const folderElement = document.getElementById(folderId);
    folderElement.className = "folder-element";
    this.folderMap.set(folderId, "folder");
    this.selectedFolders.delete(folderId);
  }

  public selectFolder(folderId: string): void {
    this.selectedFolders.forEach(currentFolderId => {
      this.unselectFolder(currentFolderId);
    });
    const folderElement = document.getElementById(folderId);
    folderElement.className = "folder-element folder-element-selected";
    this.folderMap.set(folderId, "folder_open");
    this.selectedFolders.add(folderId);
  }

  public selectFileElements(fileId: string): void {
    if (this.selectedFiles.has(fileId)) {
      this.unselectFile(fileId);
      return;
    }
    this.selectFile(fileId);
  }

  public unselectFile(fileId: string): void {
    const fileElement = document.getElementById(fileId);
    fileElement.className = "file-element";
    this.selectedFiles.delete(fileId);
  }

  public selectFile(fileId: string): void {
    this.selectedFiles.forEach(currentFileId => {
      this.unselectFile(currentFileId);
    });
    const fileElement = document.getElementById(fileId);
    fileElement.className = "file-element file-element-selected";
    this.selectedFiles.add(fileId);
  }

  public openFolderContextMenu(event: MouseEvent): void {
    this.closeFileContextMenu();
    this.contextX = event.clientX;
    this.contextY = event.clientY;
    event.preventDefault();
    const folderContextMenu = document.getElementById("folder-context-menu");
    this.render.setStyle(folderContextMenu, "display", "block");
    this.render.setStyle(folderContextMenu, "top", this.contextY + "px");
    this.render.setStyle(folderContextMenu, "left", this.contextX + "px");
  }

  public openFileContextMenu(event: MouseEvent): void {
    this.closeFolderContextMenu();
    this.contextX = event.clientX;
    this.contextY = event.clientY;
    event.preventDefault();
    const fileContextMenu = document.getElementById("file-context-menu");
    this.render.setStyle(fileContextMenu, "display", "block");
    this.render.setStyle(fileContextMenu, "top", this.contextY + "px");
    this.render.setStyle(fileContextMenu, "left", this.contextX + "px");
  }

  public closeFileContextMenu(): void {
    const fileContextMenu = document.getElementById("file-context-menu");
    this.render.setStyle(fileContextMenu, "display", "none");
  }

  public closeFolderContextMenu(): void {
    const folderContextMenu = document.getElementById("folder-context-menu");
    this.render.setStyle(folderContextMenu, "display", "none");
  }

  public showContextColorChooser(): void {
    const colorChooser = document.getElementById("context-choose-colors");
    this.render.setStyle(colorChooser, "display", "flex");
  }

  public hideContextColorChooser(): void {
    const colorChooser = document.getElementById("context-choose-colors");
    this.render.setStyle(colorChooser, "display", "none");
  }

  public getMaterialNameByType(type: string): string {
    return this.typeObject.has(type) ? this.typeObject.get(type) : this.typeObject.get("none");
  }

  public getColorByFolderObject(folder: any): string {
    return folder.color ? folder.color : this.folderColors[0];
  }

  public previewFile(fileId: string): void {
    this.router.navigate(["/cloud/" + fileId + "/preview"]);
  }

  public previewSelectedFile(): void {
    this.router.navigate(["/cloud/" + this.selectedFiles.values().next().value + "/preview"]);
  }

}
