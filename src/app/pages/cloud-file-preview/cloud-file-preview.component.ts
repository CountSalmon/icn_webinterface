import {Component, OnInit, Renderer2} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Credentials} from "../../scripts/Credentials";

@Component({
  selector: 'app-cloud-file-preview',
  templateUrl: './cloud-file-preview.component.html',
  styleUrls: ['./cloud-file-preview.component.css']
})
export class CloudFilePreviewComponent implements OnInit {

  public fileName: string = "config.json";
  public fileContent: string = "Lorem Impsum Dolor";

  /**
   * IMPORTANT: cannot use routerLink because of the AOT compiler
   * @private
   */

  private givenFileId = "";

  constructor(private render: Renderer2, private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit(): void {
    if (!Credentials.areGiven()) {
      this.router.navigate(["/login"]);
    }
    this.route.paramMap.subscribe(params => {
      this.givenFileId = params.get("fileId");
    });
  }

  public navigateTo(type: string): void {
    this.router.navigate(["/cloud/" + this.givenFileId + "/" + type]);
  }

}
