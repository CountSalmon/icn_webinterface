import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloudFilePreviewComponent } from './cloud-file-preview.component';

describe('CloudFilePreviewComponent', () => {
  let component: CloudFilePreviewComponent;
  let fixture: ComponentFixture<CloudFilePreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloudFilePreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudFilePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
