import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloudFileShareComponent } from './cloud-file-share.component';

describe('CloudFileShareComponent', () => {
  let component: CloudFileShareComponent;
  let fixture: ComponentFixture<CloudFileShareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloudFileShareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudFileShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
