import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient, HttpParams} from "@angular/common/http";
import {BackendRequest} from "../../scripts/BackendRequest";
import {MessageHandler} from "../../scripts/MessageHandler";
import {HomeComponent} from "../home/home.component";
import {Credentials} from "../../scripts/Credentials";
import {RoomHandler} from "../../scripts/RoomHandler";

@Component({
  selector: 'app-edit-room',
  templateUrl: './edit-room.component.html',
  styleUrls: ['./edit-room.component.css']
})
export class EditRoomComponent implements OnInit {

  public givenRoomId: string;
  public roomName: string;
  public roomAuthorObject: object;
  public roomObject;
  public permittedUsers = [];
  public restUsers = [];
  public newRoomName: string;
  private permittedUsersStartingSize = 0;

  constructor(private router: Router, private http: HttpClient, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.givenRoomId = params.get("roomId");
      this.setRoomData();
    });
  }

  public removePermittedUser(user: any): void {
    this.restUsers.push(user);
    const userIndex = this.permittedUsers.indexOf(user, 0);
    if (userIndex > -1) {
      this.permittedUsers.splice(user, 1);
      const removeRoomPermissionRequest: BackendRequest = new BackendRequest(
        `/api/room/permission/${this.givenRoomId}/${user.id}`,
        "delete",
          this.http,
          {},
          {},
        "json"
      ).setDefaultHeaders();
      removeRoomPermissionRequest.execute().toPromise().then(result => {
        MessageHandler.sendSuccess(`Revoked permission for ${user.name} for the room ${this.roomName}`, 1500);
      });
    }
  }

  public addUserToPermittedUsers(user: any): void {
    this.permittedUsers.push(user);
    const userIndex = this.restUsers.indexOf(user, 0);
    if (userIndex > -1) {
      this.restUsers.splice(user, 1);
      const addRoomPermissionRequest: BackendRequest = new BackendRequest(
        "/api/room/permission",
        "post",
        this.http,
        {
          params: new HttpParams()
            .set("accountId", user.id)
            .set("roomId", this.givenRoomId)
        },
        {},
        "json"
      ).setDefaultHeaders();
      addRoomPermissionRequest.execute().toPromise().then(result => {
        MessageHandler.sendSuccess(`Granted permission to ${user.name} for the room ${this.roomName}`, 1500);
      });
    }
  }

  public editRoom(newName: string): void {
    // room name has changes
    if (newName !== this.roomName) {
      const changeRoomNameRequest: BackendRequest = new BackendRequest(
        "/api/room/edit/" + this.givenRoomId,
        "post",
        this.http,
        {
          params: new HttpParams().set("newName", newName)
        },
        {},
        "json"
      ).setDefaultHeaders();
      changeRoomNameRequest.execute().toPromise().then(result => {
        MessageHandler.sendSuccess(`The roomname has changes to ${newName}`, 1500);
        HomeComponent.setSidebarData(this.http);
        this.router.navigate([`/room/${this.givenRoomId}/view`]);
      });
    }
  }

  public getAuthorName(): string {
    if (this.roomAuthorObject) {
      // @ts-ignore
      return this.roomAuthorObject.name;
    }
    return "";
  }

  private setPermittedUsers(): void {
    const getPermittedUsersRequest: BackendRequest = new BackendRequest(
      "/api/room/permission/room/" + this.givenRoomId + "/user",
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    const resultPromise = getPermittedUsersRequest.execute().toPromise();
    resultPromise.then(result => {
      // @ts-ignore
      result.forEach(user => {
          // user is not room author
          if (user.id !== this.roomObject.authorId) {
            this.permittedUsers.push(user);
          } else {
            this.roomAuthorObject = user;
          }
        }
      );
      this.permittedUsersStartingSize = this.permittedUsers.length;
      this.setAllUsers();
    });
  }

  private setAllUsers(): void {
    const getAllUsers: BackendRequest = new BackendRequest(
      "/api/user/all",
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    getAllUsers.execute().toPromise().then(result => {
      // @ts-ignore
      result.forEach(user => {
        // @ts-ignore
        if (!this.userHasPermission(user.id) && this.roomAuthorObject.id !== user.id) {
          this.restUsers.push(user);
        }
      });
    });
  }

  private userHasPermission(userId): boolean {
    let contains: boolean = false;
    this.permittedUsers.forEach(user => {
      if (user.id === userId) {
        contains = true;
      }
    });
    return contains;
  }

  private setRoomData(): void {
    const getRoomsByUserRequest: BackendRequest = new BackendRequest(
      "/api/room/" + this.givenRoomId,
      "get",
      this.http,
      {},
      {},
      "json"
    ).setDefaultHeaders();
    const resultPromise = getRoomsByUserRequest.execute().toPromise();
    resultPromise.then(result => {
      this.roomObject = result;
      // @ts-ignore
      this.roomName = result.name;
      this.newRoomName = this.roomName;
      this.setPermittedUsers();
    });
  }

}
