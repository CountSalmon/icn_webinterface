import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  public errorCodes = [
    {
      id: "111",
      content: "There were connection problems with the backend"
    },
    {
      id: "222",
      content: "The route entered does not exist"
    }
  ];

  public givenErrorContent = "";

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    let value;
    this.route.paramMap.forEach(param => {
      // @ts-ignore
      value = param.params.id;
    });
    if (value) {
      this.errorCodes.forEach(errorElement => {
        if (errorElement.id === value) {
          this.givenErrorContent = errorElement.content;
        }
      });
    }
  }

  public navigateToHome(): void {
    this.router.navigate(["home"]);
  }

}
