import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {Utils} from "../../scripts/Utils";
import {ActivatedRoute, Router} from "@angular/router";
import {MessageHandler} from "../../scripts/MessageHandler";
import {Credentials} from "../../scripts/Credentials";
import {RoomHandler} from "../../scripts/RoomHandler";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'webinterface';

  public userId;

  public getRoomDashBoardData(): any {
    return RoomHandler.givenRooms;
  }

  constructor(private titleService: Title, private router: Router, private route: ActivatedRoute) {
    Utils.setPageTitle(titleService, "HOME");
    if (!Credentials.areGiven()) {
      this.router.navigate(["/login"]);
    }
  }

  ngOnInit(): void {
    // setup the messaging system
    MessageHandler.setup();
    this.router.navigate(["/home"]);
    this.router.events.subscribe(() => {
      if (Credentials.getUser() && !this.userId) { this.userId = Credentials.getUser().id; }
    });
  }

  public getProfilePicture(): any {
    if (Credentials.getUser()) {
      return Credentials.getUser().imagePath;
    }
    return null;
  }

  public toggleSidebarDropdown(dropdownId: string, parentId: string): void {
    const dropdown = document.getElementById(dropdownId);
    const parentElement = document.getElementById(parentId);
    if (dropdown && parentElement) {
      const currentState: boolean = dropdown.hidden;
      dropdown.hidden = !currentState;
      parentElement.className = currentState ? "sidebar-element sidebar-active" : "sidebar-element sidebar-inactive";
    }
  }

  public isLoggedIn(): boolean {
    return Credentials.areGiven();
  }

  public getUserName(): string {
    return Credentials.getUser().name;
  }

}
