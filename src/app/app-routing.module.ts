import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from "./pages/root/app.component";
import {CloudComponent} from "./pages/cloud/cloud.component";
import {RoomComponent} from "./pages/room/room.component";
import {HomeComponent} from "./pages/home/home.component";
import {ErrorComponent} from "./pages/error/error.component";
import {SettingsComponent} from "./pages/settings/settings.component";
import {LoginComponent} from "./pages/login/login.component";
import {CloudFilePreviewComponent} from "./pages/cloud-file-preview/cloud-file-preview.component";
import {CloudFileRenameComponent} from "./pages/cloud-file-rename/cloud-file-rename.component";
import {CloudFileShareComponent} from "./pages/cloud-file-share/cloud-file-share.component";
import {CloudFileShareLinkComponent} from "./pages/cloud-file-share-link/cloud-file-share-link.component";
import {CloudFileDeleteComponent} from "./pages/cloud-file-delete/cloud-file-delete.component";
import {CloudFileDetailsComponent} from "./pages/cloud-file-details/cloud-file-details.component";
import {CloudFileStopSharingComponent} from "./pages/cloud-file-stop-sharing/cloud-file-stop-sharing.component";
import {CallComponentComponent} from "./pages/call-component/call-component.component";
import {ProfileViewComponent} from "./pages/profile-view/profile-view.component";
import {ProfileEditComponent} from "./pages/profile-edit/profile-edit.component";
import {ProfileListComponent} from "./pages/profile-list/profile-list.component";
import {DeleteRoomComponent} from "./pages/delete-room/delete-room.component";
import {EditRoomComponent} from "./pages/edit-room/edit-room.component";
import {ProfileRemoveComponent} from "./pages/profile-remove/profile-remove.component";
import {CreateRoomComponent} from "./pages/create-room/create-room.component";
import {CreateRoomComponentComponent} from "./pages/create-room-component/create-room-component.component";


const routes: Routes = [
  {
    path: "",
    component: AppComponent
  },
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "cloud",
    component: CloudComponent
  },
  {
    path: "cloud/:fileId/preview",
    component: CloudFilePreviewComponent,
  },
  {
    path: "cloud/:fileId/share",
    component: CloudFileShareComponent,
  },
  {
    path: "cloud/:fileId/shareLink",
    component: CloudFileShareLinkComponent
  },
  {
    path: "cloud/:fileId/delete",
    component: CloudFileDeleteComponent
  },
  {
    path: "cloud/:fileId/details",
    component: CloudFileDetailsComponent
  },
  {
    path: "cloud/:fileId/rename",
    component: CloudFileRenameComponent
  },
  {
    path: "cloud/:fileId/stopSharing",
    component: CloudFileStopSharingComponent
  },
  {
    path: "room/create",
    component: CreateRoomComponent
  },
  {
    path: "room/:roomId/view",
    component: RoomComponent
  },
  {
    path: "room/:roomId/delete",
    component: DeleteRoomComponent
  },
  {
    path: "room/:roomId/edit",
    component: EditRoomComponent
  },
  {
    path: "room/:roomId/create",
    component: CreateRoomComponentComponent
  },
  {
    path: "settings",
    component: SettingsComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "error/:id",
    component: ErrorComponent
  },
  {
    path: "call/component/:roomId/:componentId",
    component: CallComponentComponent
  },
  {
    path: "profile/:accountId/view",
    component: ProfileViewComponent
  },
  {
    path: "profile/:accountId/edit",
    component: ProfileEditComponent
  },
  {
    path: "profile/:accountId/delete",
    component: ProfileRemoveComponent
  },
  {
    path: "profile/list",
    component: ProfileListComponent
  },
  {
    path: "**",
    component: ErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
