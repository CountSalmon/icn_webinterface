import {Title} from '@angular/platform-browser';

export class Utils {

  public static titlePrefix: string = "ICN - ";

  public static setPageTitle(titleService: Title, newTitle: string): void {
    titleService.setTitle(this.titlePrefix + newTitle);
  }

  public static isURL(content: string): boolean {
    const pattern = new RegExp("https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)");
    return pattern.test(content);
  }


}
