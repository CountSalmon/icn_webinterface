export class MessageHandler {

  private static typeColors = {
    error: "#861d1d",
    info: "#1f4287",
    success: "#106610",
    warning: "#dbb845"
  };

  public static setup(): void {
    MessageHandler.getCloseButton().onclick = () => {
      MessageHandler.popDown();
    };
  }

  public static sendInfo(message: string, delay: number): void {
    this.sendMessage("info", "INFO", message, delay);
  }

  public static sendError(message: string, delay: number): void {
    MessageHandler.sendMessage("error", "An error occured", message, delay);
  }

  public static sendSuccess(message: string, delay: number): void {
    MessageHandler.sendMessage("success", "INFO", message, delay);
  }

  public static sendWarning(message: string, delay: number): void {
    MessageHandler.sendMessage("warning", "WARNING", message, delay);
  }

  private static async sendMessage(type: string, title: string, message: string, delay: number): Promise<void> {
    MessageHandler.getPopup().style.backgroundColor = MessageHandler.typeColors[type];
    MessageHandler.getTitleElement().innerText = title;
    MessageHandler.getMessageElement().innerText = message;
    MessageHandler.popUp();
    setTimeout(MessageHandler.popDown, delay);
  }

  private static getPopup(): HTMLElement {
    return document.getElementById("sidebar-popup");
  }

  private static getTitleElement(): HTMLElement {
    return document.getElementById("sidebar-popup-title");
  }

  private static getMessageElement(): HTMLElement {
    return document.getElementById("sidebar-popup-message");
  }

  private static getCloseButton(): HTMLElement {
    return document.getElementById("sidebar-popup-close");
  }

  private static popUp(): void {
    MessageHandler.getPopup().style.transform = "translateY(-8rem)";
  }

  private static popDown(): void {
    MessageHandler.getPopup().style.transform = "translateY(0)";
  }

}
