/**
 * The static class to store the current credentials
 */
import {HttpClient} from "@angular/common/http";

export class Credentials {

  private static username: string;
  private static password: string;
  private static givenUser: any;

  /**
   * checks if username and password are not null and not empty
   */
  public static areGiven(): boolean {
    return !!(Credentials.password && Credentials.username);
  }

  /**
   * getter for the username
   */
  public static getUserName(): string {
    return Credentials.username;
  }

  /**
   * setter for the username
   * @param newId given Id
   */
  public static setUserName(newId: string): void {
    Credentials.username = newId;
  }

  /**
   * getter for the password
   */
  public static getPassword(): string {
    return Credentials.password;
  }

  /**
   * setter for the password
   * @param password given password
   */
  public static setPassword(password: string): void {
    Credentials.password = password;
  }

  /**
   * setter for the given user
   * @param user given user
   */
  public static setUser(user: any): void {
    Credentials.givenUser = user;
  }

  /**
   * getter for the given user
   */
  public static getUser(): any {
    return Credentials.givenUser;
  }

}
