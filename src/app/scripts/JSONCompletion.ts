export class JSONCompletion {

  private static completionObject: object = [
    {
      first: "{",
      completion: "}"
    },
    {
      first: "[",
      completion: "]"
    },
    {
      first: '"',
      completion: '": ""'
    }
  ];

  public static getOutputKey(key: string): string {
    let outputValue = "";
    // @ts-ignore
    this.completionObject.forEach(element => {
      if (key === element.first) {
        outputValue = element.completion;
      }
    });
    return outputValue;
  }

}
