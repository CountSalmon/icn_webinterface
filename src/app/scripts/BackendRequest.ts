import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Credentials} from "./Credentials";
import {Observable} from "rxjs";

@Injectable()
export class BackendRequest {

  private static backendRoute = "http://localhost:3000";

  public static getCredentials(): string {
    return btoa(Credentials.getUserName()) + ":" + btoa(Credentials.getPassword());
  }

  // tslint:disable-next-line:max-line-length
  constructor(private route: string, private method: string, private http: HttpClient, private options: any, private body: any, private responseType: string) {
    this.options.responseType = responseType;
  }

  public execute(): Observable<ArrayBuffer> {
    switch (this.method.toLowerCase()) {
      case "post":
        return this.http.post(BackendRequest.backendRoute + this.route, this.body, this.options);
      case "put":
        return this.http.put(BackendRequest.backendRoute + this.route, this.body, this.options);
      case "delete":
        return this.http.delete(BackendRequest.backendRoute + this.route, this.options);
      case "get":
      default:
        return this.http.get(BackendRequest.backendRoute + this.route, this.options);
    }
  }

  public setDefaultHeaders(): BackendRequest {
    this.options.headers = new HttpHeaders().set("credentials", BackendRequest.getCredentials());
    return this;
  }

  public getBody(): object {
    return this.body;
  }

  public getMethod(): string {
    return this.method;
  }

  public getRoute(): string {
    return this.route;
  }

  public getOptions(): object {
    return this.options;
  }

  public getHeaders(): HttpHeaders {
    return this.options.headers;
  }

  public appendToHeader(key: string, value: string): void {
    this.options.headers = this.getHeaders().append(key, value);
  }

}
