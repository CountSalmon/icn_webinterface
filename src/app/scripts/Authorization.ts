import {Router} from "@angular/router";
import {Credentials} from "./Credentials";
import {Mode} from "./Mode";

export class Authorization {

  public static checkForLogin(router: Router): boolean {
    if (Mode.mode.toLowerCase() !== "test") {
      if (!Credentials.areGiven()) {
        router.navigate(["/login"]);
        return false;
      }
      return true;
    }
    return true;
  }
}
